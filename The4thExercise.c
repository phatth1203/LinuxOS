/* kỹ thuật này sẽ ghi 1 byte vào file descriptor cho mỗi child process
được sinh ra mà parent process
có thể đọc được.
*/
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#define NumOfProcess 1239
int main(){
int i,fd1[2],fd2[2];
int depth = 0; //Theo dõi số lượng child process tạo ra.
 pipe(fd1); // tạo một ống dẫn sẽ chứa các process con được tạo.
 pipe(fd2); // tạo ống dẫn chứa các process tạo không thành công.
for(i=0;i<NumOfProcess;i++) {
	int z = fork();
	if(z == 0){  // fork() trả về 0 nếu đây là child process.
		write(fd1[1],&i,1); // ghi 1 byte vào ống dẫn.
		depth+=1;
		exit(0); 
		}
	if(z < 0) {  // fork() trả về số âm khi tạo process thất bại.
		write(fd2[1],&i,1); // ghi 1 byte vào ống dẫn.			
	}
}
	
for(i=0;i<NumOfProcess;i++){
	wait(NULL); // đợi 1239 child process.
	}
close(fd1[1]);
close(fd2[1]);  
    if( depth == 0 ) {  // đây là parent process.
      i=0;
      while(read(fd1[0],&depth,1) != 0)
        i += 1;
        // in ra tổng số process tạo thành công
      printf( "%d total processes spawned \n", i);  
 
	 i=0;
      while(read(fd2[0],&depth,1) != 0)
        i += 1;
        // in ra tổng số process tạo thất bại
      printf( "%d total processes failed \n", i);
    }
	
    return 0;
}