#include <stdio.h>
#include <dirent.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#define numFork 3
#define MAX 50
#define NumofPipe 10
// tạo 10 pipe để chứa các nội dụng đọc được từ process 1
int fd[2*NumofPipe];   
// tạo một pipe để gửi số lượng file sang process 2
int fd1[2];
// thread process 1 để đọc dữ liệu và lưu vào saved
void *thread(char* filename,char* saved){
FILE *fi;
fi = fopen(filename,"r");
char buff[MAX];
if(!fi)
	printf("Can not open %s\n",filename);
else
	fgets(buff,MAX,(FILE*)fi);
	int z;
	strcpy(saved,buff);
fclose(fi);
}
// thread process 2 để tìm các string xuất hiện trong file
void *thread2(char* str,char* saved){
 int len = strlen(saved);
 // return NULL nếu str không có trong saved
 if(strstr(saved,str)!= NULL){
     //tạo và chép vào file data.txt
 FILE* ft;
 ft = fopen("data.txt","r+");
 fputs(str,ft);
 fclose(ft);
 }
}

int main()
{ 
    // lấy input để kiểm tra các string trong file
	char input_str[MAX];
	printf("input: ");
	fflush(stdin);
	scanf("%s",input_str);
int m;
// tạo 10 pipe
for(m=0;m<NumofPipe;m++){
 pipe(&fd[2*m]);
}
pipe(fd1);

int status,i;

pid_t p; // tạo fork() p

for(i=0;i<3;i++){
	p = fork();
	if(p == 0){
	// process 1 
	if(i == 0){ 
	    // đọc dữ liệu từ dictionary 
	DIR *d;
	char* filename[MAX];
        struct dirent *dir;	
        d = opendir(".");
 		if (d){
 		    //khởi tạo một thread
 			pthread_t p;
 			int filenum = 0;
            while ((dir = readdir(d)) != NULL){
            filename[filenum] = dir->d_name;
            if(strstr(dir->d_name,".txt") !=NULL)
            filenum+=1;
			}
			int z;
			// ghi số lượng file dạng .txt đọc được vào pipe
			close(fd1[0]);
			write(fd1[1],&filenum,4);
			close(fd1[1]);
			char data[50];
			//multithreading
			for(z=0;z<filenum;z++){  
			char *t = filename[z];
			//kiểm tra file có phải dạng .txt
			if(strstr(t,".txt") !=NULL){ 
			pthread_create(&p,NULL,thread(t,data),NULL);
			printf("Text in file %s :%s",t,data);
			// gửi dữ liệu vào pipe để process2 có thể đọc
			close(fd[2*z]);
			write(fd[2*z+1],data,strlen(data)+1);
			close(fd[2*z+1]);
			}
			}
			
            		closedir(d);
		}
		exit(0);
	}
	//process2
	else if(i == 1){ 
	usleep(5000);
	
	FILE *ft0;
	// tạo file data.txt để ghi chuỗi tìm được
	ft0 = fopen("data.txt","w");
	fclose(ft0);
	int filenum1;
	// đọc số lượng file đếm được từ process 1
	close(fd1[1]);
	read(fd1[0],&filenum1,4);
	close(fd1[0]);
	char saved[MAX];
	pthread_t p2;
	int z;
	//mutilthreading
	for(z=0;z<filenum1;z++){
	// đọc dữ liệu trong file thông qua pipe mà process 1 đã đọc
	close(fd[2*z+1]);
	read(fd[2*z],saved,MAX);
	close(fd[2*z]);
	// mỗi vòng lặp sẽ đọc dữ liệu trong 1 file
	pthread_create(&p2,NULL,thread2(input_str,saved),NULL);
	}
	exit(0);
	}
	//process3
	else if( i == 2){  
	usleep(50000);
	char buff[100];
	//đọc file data.txt  và in lên màng hình
	FILE *f3;
	f3 = fopen("data.txt","r");
	fgets(buff,100,(FILE*)f3);
	printf("text in data.txt :%s\n", buff);	
	exit(0);
	}
	}
	if(p > 0) {
	int k;
	
}
}
// chờ các childprocess
for(i=0;i<3;i++)
	wait(NULL);
	return 0;
} 
